from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('download-report/', views.download_summary, name="download_summary"),
    path('download-profit-data/', views.download_profit_data, name="download_profit_data"),
]
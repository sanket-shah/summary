from django.apps import AppConfig


class NetProfitConfig(AppConfig):
    name = 'net_profit'

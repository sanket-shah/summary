from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import messages
import io
import pandas as pd
import csv
ANALYSIS_DONE=True

def parse_reports(data,price_data):
    data = data[['(Parent) ASIN', 'Title', 'Units Ordered']]
    data=data.rename(columns={"(Parent) ASIN": "product_id"})
    price_data['tax'] = price_data['cost_price'] * 0.12
    price_data['cost_price_with_tax'] = price_data['cost_price'] + price_data['tax']
    price_data['delivery_charges'] = [65] * len(price_data)
    price_data['customer_pays'] = price_data['item_price'] + price_data['delivery_charges']
    price_data['referral_fee'] = price_data['customer_pays'] * 0.05
    price_data.loc[price_data['customer_pays'] > 300,'referral_fee'] = price_data['customer_pays'] * 0.115
    price_data['fixed_fee'] = [5] * len(price_data)
    price_data['fee_discount'] = [0] * len(price_data)
    price_data['total_fee'] = price_data['fixed_fee'] + price_data['referral_fee'] - price_data['fee_discount']
    price_data['gst_on_fee'] = price_data['total_fee'] * 0.18
    price_data['fee_with_tax'] = price_data['total_fee']+price_data['gst_on_fee']
    price_data['net_selling_price'] = price_data['customer_pays'] - price_data['fee_with_tax']
    price_data['net_proceeds_after_shipping'] = price_data['net_selling_price'] - price_data['delivery_charges']
    price_data['product_gst'] = price_data['item_price']*0.1025
    price_data['net_proceeds_after_tax'] = price_data['net_proceeds_after_shipping'] - price_data['product_gst']
    price_data['net_profit'] = price_data['net_proceeds_after_tax']-price_data['cost_price_with_tax']
    price_data.to_csv("net_profit_of_products.csv",index=False)

    data = data.merge(price_data, on='Title')
    data = data[['product_id', 'Title', 'Units Ordered', 'net_profit']]
    data['total_profit'] = data['Units Ordered']*data['net_profit']
    data.to_csv("summary.csv",index=False)
    ANALYSIS_DONE=True
    return data,price_data

def index(request):
    template =  "net_profit/amazon.html"
    if request.method=="GET":
        return render(request,template)
    csv_file_1= request.FILES['file_1']
    print(csv_file_1)
    if not csv_file_1.name.endswith(".csv"):
        messages.error(request,"This is not a csv file")
    data = pd.read_csv(csv_file_1)

    csv_file_2 = request.FILES['file_2']
    if not csv_file_2.name.endswith(".csv"):
        messages.error(request, "This is not a csv file")
    price_data = pd.read_csv(csv_file_2)
    data,price_data = parse_reports(data,price_data)


    return render(request,template,{"show_download":ANALYSIS_DONE})
# Create your views here.

def download_summary(request):
  data = pd.read_csv('summary.csv')
  response = HttpResponse(content_type='text/csv')
  response['Content-Disposition'] = 'attachment; filename=summary.csv'
  data.to_csv(path_or_buf=response,sep=',',float_format='%.2f',index=False,decimal=".")
  return response

def download_profit_data(request):
  data = pd.read_csv('net_profit_of_products.csv')
  response = HttpResponse(content_type='text/csv')
  response['Content-Disposition'] = 'attachment; filename=net_profit_of_products.csv'
  data.to_csv(path_or_buf=response,sep=',',float_format='%.2f',index=False,decimal=".")
  return response